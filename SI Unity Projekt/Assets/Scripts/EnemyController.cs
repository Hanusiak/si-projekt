﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public GameObject BulletPrefab;
	Transform child;
	Transform AimStartPoint;
	Transform BulletStartPoint, BulletMiddlePoint;
	float fireRateTimer;
	public int damage = 0;
	public float fireRate = 1f;
	public int health = 5;

	void Start () {
		child = transform.GetChild (0);
		AimStartPoint = transform.GetChild (1);
		BulletStartPoint = child.GetChild (0);
		BulletMiddlePoint = child.GetChild (1);

		fireRateTimer = 10f;
	}

	void Update () {
		if (!HeroController._instance.isDead && GameController._instance.isGameStarted && Vector3.Distance (transform.position, HeroController._instance.transform.position) < OverallSettings._instance.enemyMinDistFromPlayerToAttack) {
			TurnTowardsHero ();
			Shoot ();
		}
	}

	void TurnTowardsHero()
	{
		float aimingAngle = -Mathf.Atan2 ((HeroController._instance.transform.position.y - AimStartPoint.transform.position.y + 1.5f), (HeroController._instance.transform.position.x - AimStartPoint.transform.position.x)) * Mathf.Rad2Deg;

		if (HeroController._instance.gameObject.transform.position.x <= child.position.x) {
			child.localScale = new Vector3 (-1f, 1f, 1f);
			aimingAngle += 180;
			if (aimingAngle > 40f && aimingAngle < 90f)
				aimingAngle = 40f;
			else if (aimingAngle < 320f && aimingAngle > 270f)
				aimingAngle = 320f;

		} else {
			child.localScale = new Vector3 (1f, 1f, 1f);
			if (aimingAngle > 40f && aimingAngle < 90f)
				aimingAngle = 40f;
			else if (aimingAngle < -40f && aimingAngle > -90f)
				aimingAngle = -40f;
		}

		
		child.transform.localEulerAngles = new Vector3 (0f, aimingAngle, 0f);
	}

	void Shoot() {

		fireRateTimer += Time.deltaTime;

		Vector3 heading = BulletMiddlePoint.position - BulletStartPoint.position;
		float distance = heading.magnitude;
		Vector3 normalizedDirection = heading / distance;

		Debug.DrawRay (BulletStartPoint.position, normalizedDirection * 3f, Color.red);

		RaycastHit hit;
		if (Physics.Raycast (BulletStartPoint.position, normalizedDirection, out hit)) {
			if (hit.collider.gameObject.tag == "Player") {
				if (fireRateTimer >= fireRate) {
					fireRateTimer = 0f;

					BulletStartPoint.transform.LookAt (OverallSettings._instance.BulletMiddlePoint);
					GameObject bullet = (GameObject)Instantiate (BulletPrefab, BulletStartPoint.transform.position, new Quaternion ());
					bullet.GetComponent<Projectile> ().Shoot (BulletMiddlePoint, 2300, this.gameObject);
				}
			}
		}
	}

	public void TakeDamage(int damage)
	{
		health -= damage;
		if (health <= 0) {
			GameController._instance.generalScore += 10;
			Destroy (this.gameObject);
		}
	}
}

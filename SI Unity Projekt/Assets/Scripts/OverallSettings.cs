﻿using UnityEngine;

public class OverallSettings : MonoBehaviour{

	public int heroMaxHealth = 50;
	public float regenerationTimer = 5f;
	public int additionalHealth = 2;
	public int heroLivesAtStart = 3;
	public int heroBulletForce = 1000;
	public float enemyMinDistFromPlayerToAttack = 10f;

	public float yGravity = -2f;

	public GameObject BlueHitPrefab;
	public GameObject RedHitPrefab;

	public Transform BulletMiddlePoint;

	public static OverallSettings _instance;

	void Start()
	{
		_instance = this;
		Physics.gravity = new Vector3(0f, yGravity , 0f);
	}
}

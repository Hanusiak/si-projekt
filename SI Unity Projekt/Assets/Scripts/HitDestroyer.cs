﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitDestroyer : MonoBehaviour {

	void Start () {
		StartCoroutine ("DestroyMe");
	}

	IEnumerator DestroyMe()
	{
		yield return new WaitForSeconds (1f);
		Destroy (this.gameObject);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesController : MonoBehaviour {

	public GameObject enemiesParent;
	public GameObject GreenTurretPrefab, YellowTurretPrefab, RedTurretPrefab;
	public float minDistanceBetweenEnemies = 5f;
	List<GameObject> enemies;

	public static EnemiesController _instance;

	void Start () {
		_instance = this;
		enemies = new List<GameObject> ();
	}

	//ustawienie wrogów na mapie
	public void SetEnemiesOnTheMap(Vector3 entryPortalPosition, Vector3 exitPortalPosition)
	{
		List<Vector2> allPossiblePlacesForEnemy = GetAllHorizontalBlocksIndices ();
		List<Vector2> chosenPlacesForEnemy = new List<Vector2> ();

		//POSZUKIWANIE MIEJSC DO WSTAWIENIA WROGÓW
		int placesFound = 0;
		int allPlacesToFind = GameController._instance.currentLevel.lvlGreenTurretNumber + GameController._instance.currentLevel.lvlYellowTurretNumber + GameController._instance.currentLevel.lvlRedTurretNumber;

		while (placesFound < allPlacesToFind) {
			int randomIndex = Random.Range (0, allPossiblePlacesForEnemy.Count);	//wylosowanie indeksu ze wszystkich możliwych miejsc
			Vector2 randomPos = allPossiblePlacesForEnemy [randomIndex];			//przypisanie tej pozycji do randomPos
			randomPos = new Vector2 ((randomPos.x * 2f) + 1f, randomPos.y + 0.5f);	//zmiana z wartości indeksu tablicy na rzeczywistą pozycję na scenie, na razie 2D

			allPossiblePlacesForEnemy.RemoveAt (randomIndex);						//usunięcie z listy tego miejsca

			bool isDistanceSafe = true;

			//sprawdza, czy dystans między już wybranymi miejscami, a tym obecnym nie jest za mały
			foreach (Vector2 setPlace in chosenPlacesForEnemy) {
				if (Vector2.Distance (setPlace, randomPos) < minDistanceBetweenEnemies)
					isDistanceSafe = false;
			}

			//sprawdza, czy dystans pomiędzy wybranym miejscem, a portalami nie jest za mały
			if((Vector2.Distance(randomPos, entryPortalPosition) < minDistanceBetweenEnemies) || (Vector3.Distance (randomPos, exitPortalPosition) < minDistanceBetweenEnemies))
				isDistanceSafe = false;

			if (isDistanceSafe) {	//jeśli dystans nie okazał się za mały, to dodaje to miejsce do listy wybranych miejsc
				chosenPlacesForEnemy.Add (randomPos);
				placesFound++;
			}
		}

		//USTAWIENIE WROGÓW NA MAPIE

		//zielone
		for (int i = 0; i < GameController._instance.currentLevel.lvlGreenTurretNumber; i++) {
			int randomIndex = Random.Range (0, chosenPlacesForEnemy.Count);		//wylosowanie indeksu ze wszystkich finalnych pozycji wrogów
			Vector3 enemyPos = new Vector3(chosenPlacesForEnemy[randomIndex].x, chosenPlacesForEnemy[randomIndex].y, MapMaker._instance.zCoordOfTheMap - 1f);
			GameObject newEnemy = (GameObject)Instantiate (GreenTurretPrefab, enemyPos, new Quaternion(), enemiesParent.transform);
			newEnemy.transform.localEulerAngles = new Vector3 (-90f, 0, 0f);
			enemies.Add (newEnemy);
			chosenPlacesForEnemy.RemoveAt (randomIndex);
		}

		//żółte
		for (int i = 0; i < GameController._instance.currentLevel.lvlYellowTurretNumber; i++) {
			int randomIndex = Random.Range (0, chosenPlacesForEnemy.Count);		//wylosowanie indeksu ze wszystkich finalnych pozycji wrogów
			Vector3 enemyPos = new Vector3(chosenPlacesForEnemy[randomIndex].x, chosenPlacesForEnemy[randomIndex].y, MapMaker._instance.zCoordOfTheMap - 1f);
			GameObject newEnemy = (GameObject)Instantiate (YellowTurretPrefab, enemyPos, new Quaternion(), enemiesParent.transform);
			newEnemy.transform.localEulerAngles = new Vector3 (-90f, 0, 0f);
			enemies.Add (newEnemy);
			chosenPlacesForEnemy.RemoveAt (randomIndex);
		}

		//czerwone
		for (int i = 0; i < GameController._instance.currentLevel.lvlRedTurretNumber; i++) {
			int randomIndex = Random.Range (0, chosenPlacesForEnemy.Count);		//wylosowanie indeksu ze wszystkich finalnych pozycji wrogów
			Vector3 enemyPos = new Vector3(chosenPlacesForEnemy[randomIndex].x, chosenPlacesForEnemy[randomIndex].y, MapMaker._instance.zCoordOfTheMap - 1f);
			GameObject newEnemy = (GameObject)Instantiate (RedTurretPrefab, enemyPos, new Quaternion(), enemiesParent.transform);
			newEnemy.transform.localEulerAngles = new Vector3 (-90f, 0, 0f);
			enemies.Add (newEnemy);
			chosenPlacesForEnemy.RemoveAt (randomIndex);
		}

	}

	List<Vector2> GetAllHorizontalBlocksIndices()
	{
		List<Vector2> allHorizontalBlocksIndices = new List<Vector2> ();

		for (int x = 0; x < MapMaker._instance.roomWidth; x++) {
			for (int y = 0; y < MapMaker._instance.roomHeight; y++) {
				if (MapMaker._instance.currentMap.tableOfValues [x, y] == BlockType.Horizontal)
					allHorizontalBlocksIndices.Add (new Vector2 (x, y));
			}
		}

		return allHorizontalBlocksIndices;
	}

	public void DestroyAllEnemies()
	{
		foreach (GameObject enemy in enemies) {
			Destroy (enemy);
		}
		enemies.Clear ();
	}
}
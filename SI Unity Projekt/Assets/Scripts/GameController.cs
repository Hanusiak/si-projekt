﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public GameObject GamePanel, PausePanel, EndPanel;
	public RectTransform UITeleportalNavigatorRect;
	public Text UIGeneralScoreText, UILvlNumber, UIGameOverText;
	int gameState;				//stan gry: 1 - menu początkowe, 2 - gra, 3 - pauza, 4 - menu końcowe 
	public bool isGameStarted;
	Level[] levels;				//tablica z informacjami o poziomach
	public Level currentLevel;	//obecny poziom gry
	public int lvlNumber;		//numer obecnego poziomu
	public int generalScore;
	public bool portalAchieved = false;

	public static GameController _instance;

	void Start () {
		_instance = this;
		InitializeLevels ();
		InitializeGame ();
	}

	void InitializeLevels()		//inicjacja tablicy poziomów	
	{
		//TODO zmienić
		levels = new Level[5];
		levels [0] = new Level (1, 2, 2f, 8, 0, 0);
		levels [1] = new Level (2, 2, 2f, 8, 5, 0);
		levels [2] = new Level (3, 2, 2f, 5, 8, 0);
		levels [3] = new Level (4, 2, 2f, 3, 3, 5);
		levels [4] = new Level (5, 2, 2f, 0, 0, 10);
	}

	void InitializeGame()
	{
		ChangeGameState (2);
		RestartLevelAndScores ();
	}

	void RestartLevelAndScores()
	{
		generalScore = 0;
		lvlNumber = 1;
		ChangeLevel (lvlNumber);
		ActualizeScoresOnTheScreen();
	}

	void Update () {
		SetNavigator ();

		if (gameState == 2 && Input.GetKeyDown (KeyCode.Escape))
			ChangeGameState(3);

		UpdateLevel ();
	}

	public void ChangeGameState(int newGameState)
	{
		gameState = newGameState;

		switch (gameState) 
		{
		case 2:		//gra
			isGameStarted = true;
			GamePanel.SetActive (true);
			PausePanel.SetActive (false);
			EndPanel.SetActive (false);
			break;
		case 3:		//pauza
			isGameStarted = false;
			GamePanel.SetActive (true);
			PausePanel.SetActive (true);
			EndPanel.SetActive (false);
			break;
		case 4:		//menu końcowe
			isGameStarted = false;
			GamePanel.SetActive (true);
			PausePanel.SetActive (false);
			EndPanel.SetActive (true);

			HandleEnd ();
			break;
		}
	}

	void UpdateLevel()	//aktualizacja poziomu, jeśli potrzebna
	{
		if (portalAchieved)
		{
			Camera.main.GetComponent<CameraMoveScript> ().SetBiggerDelay ();
			lvlNumber++;						//zwiększenie numeru poziomu
			if (lvlNumber > levels.Length) 		//jeśli numer poziomu większy niż liczba poziomów to wygrana!
				ChangeGameState (4);
			else ChangeLevel(lvlNumber);		//w innym wypadku, zmiana poziomu

			portalAchieved = false;
		}
	}

	void ChangeLevel(int newLevel)
	{
		currentLevel = levels [newLevel - 1];
		UILvlNumber.text = "LVL " + currentLevel.lvlNumber.ToString ();		//zmiana podpisu z numerem poziomu

		MapMaker._instance.GenerateLevelMap();	//wygenerowanie nowej mapy z portalami, wrogami i ustawieniem gracza
	}

	public void ActualizeScoresOnTheScreen()
	{
		UIGeneralScoreText.text = generalScore.ToString ();
	}

	void HandleEnd ()	//obsługa końcowego panelu
	{
		if (lvlNumber > levels.Length)
			UIGameOverText.text = "Y O U  W O N !!!";
		else UIGameOverText.text = "G A M E  O V E R";
	}

	public void ResumeGame()
	{
		ChangeGameState (2);
	}

	public void GoToMainMenu()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("SecondScene");
	}

	public void ExitGame()
	{
		Application.Quit ();
	}

	public void SetNavigator()
	{
		if (MapMaker._instance.ExitPortal != null) {
			float aimingAngle = Mathf.Atan2 ((MapMaker._instance.ExitPortal.transform.position.y - HeroController._instance.gameObject.transform.position.y), (MapMaker._instance.ExitPortal.transform.position.x - HeroController._instance.gameObject.transform.position.x)) * Mathf.Rad2Deg - 90;
			UITeleportalNavigatorRect.eulerAngles = new Vector3 (0f, 0f, aimingAngle);
		}
	}
}
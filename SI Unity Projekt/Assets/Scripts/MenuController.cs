﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	void Update()
	{
		if (Input.GetKeyUp (KeyCode.Escape))
			Application.Quit ();
	}

	public void StartTheGame()
	{
		SceneManager.LoadScene ("Scene");
	}

	public void ExitTheGame()
	{
		Application.Quit ();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	Rigidbody rb;
	GameObject parent;

	public void Shoot(Transform BulletMiddlePoint, int bulletForce, GameObject parent)
	{
		this.parent = parent;
		if (parent.gameObject != null) {
			if (parent.layer == 8) //strzela player
				this.gameObject.layer = 11;		//to warstwa pocisku playera
			else if (parent.layer == 9) //strzela wróg
				this.gameObject.layer = 12;		//to warstwa pocisku wroga
		}
		GetComponent<Rigidbody>().AddForce((BulletMiddlePoint.transform.position - transform.position) * bulletForce);

		StartCoroutine ("DestroyMe");
	}

	void OnCollisionEnter(Collision collision){
		Instantiate(OverallSettings._instance.BlueHitPrefab, transform.position, new Quaternion());
		if (parent.gameObject != null) {
			if (parent.layer == 8) {	//strzela player
				if (collision.collider.gameObject.layer == 9) { //trafia wroga
					collision.collider.gameObject.GetComponent<EnemyController> ().TakeDamage (5);
					GameController._instance.ActualizeScoresOnTheScreen ();
				}
			} else if (parent.layer == 9) { //strzela wróg
				if (collision.collider.gameObject.layer == 8) { //trafia gracza
					int damage = parent.GetComponent<EnemyController> ().damage;
					collision.collider.gameObject.GetComponent<HeroController> ().TakeDamage (damage);
				}
			}
		}

		Destroy (this.gameObject);
	}
		
	IEnumerator DestroyMe()
	{
		yield return new WaitForSeconds (4f);
		Destroy (this.gameObject);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor.Animations;

public class HeroController : MonoBehaviour {

	//Animation
	Animator anim;
	public GameObject AimFromPoint;

	//Move
	Rigidbody rb;
	public float walkingSpeed = 1f;
	public float runningSpeed = 1f;
	public float flyingSpeed = 1f;

	public bool isFlying = false;
	public bool isArmed = false;
	public bool isWalkingBackwards = false;

	public float flyingRayMaxDistance = 1f;


	//Life&Health
	public int currentHealth;
	public Slider healthSlider;
	public bool isDead;
	float regenerationTimer;
	bool isRegenerationTimerOn;
	public Text UILivesNumber;
	public int livesLeft;

	public GameObject DamagedRobot;

	public GameObject BulletStartPoint;
	public GameObject BulletPrefab;

	float fireRateTimer;

	public static HeroController _instance;

	void Start () {
		_instance = this;

		anim = GetComponent<Animator> ();
		rb = GetComponent<Rigidbody> ();

		RestartHero(true);
	}

	void Update ()
	{
		if (GameController._instance.isGameStarted) {
			AnimateHero ();
			HealthControl ();
			ArmHero ();
			Shoot ();
		}
	}

	void FixedUpdate()
	{	if (GameController._instance.isGameStarted) {
			MoveHero ();
			RotateHero ();
		}
	}

	void MoveHero()
	{
		float h = Input.GetAxisRaw ("Horizontal");
		Vector3 movement = new Vector3 (h, 0f, 0f);

		float speed = runningSpeed;
		if (isArmed)
			speed = walkingSpeed;

		if (isFlying)
			speed = flyingSpeed - 5f;
		
		movement = movement.normalized * speed * Time.deltaTime;
		rb.MovePosition (transform.position + movement);

		//flying
		float v = Input.GetAxisRaw ("Vertical");
		if (v > 0f) {
			rb.AddForce (Vector3.up * flyingSpeed * 1000 * Time.deltaTime * v);
			isFlying = true;
		} else if (v < 0f) {
			rb.AddForce (Vector3.down * -flyingSpeed * 1000 * Time.deltaTime * v);
		}

		FlyingRayCheck ();
	}

	void AnimateHero()
	{
		if (isFlying) {
			anim.SetBool ("IsFlying", true);
		} else {
			anim.SetBool ("IsFlying", false);

			float h = Input.GetAxisRaw ("Horizontal");
			if (h != 0f)
				anim.SetBool ("IsRunning", true);
			else anim.SetBool ("IsRunning", false);

			//sprawdzenie, czy iść do tyłu
			if ((h < 0f && Mathf.RoundToInt (transform.eulerAngles.y) == 90) || (h > 0f && Mathf.RoundToInt (transform.eulerAngles.y) == 270)) {
				isWalkingBackwards = true;
				anim.SetBool ("IsWalkingBackwards", true);
			} else {
				isWalkingBackwards = false;
				anim.SetBool ("IsWalkingBackwards", false);
			}

		}
	}

	void RotateHero()
	{
		if (isArmed)
			TurnArmedHeroTowardsMousePosition ();
		else
			TurnHeroWhileRunning ();
	}

	void TurnHeroWhileRunning ()
	{
		if (Input.GetAxisRaw ("Horizontal") < 0f) {
			Vector3 newRotation = new Vector3 (0f, -90f, 0f);
			transform.localEulerAngles = newRotation;
		} else if (Input.GetAxisRaw ("Horizontal") > 0f){
			Vector3 newRotation = new Vector3 (0f, 90f, 0f);
			transform.localEulerAngles = newRotation;
		}
	}

	void TurnArmedHeroTowardsMousePosition()
	{
		Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z - Camera.main.transform.position.z));

		//ewentualny obrót postaci
		if (mouseWorldPosition.x > transform.position.x) {//w prawo
			transform.eulerAngles = new Vector3 (transform.rotation.eulerAngles.x, 90f, transform.rotation.eulerAngles.z);
		} else {//w lewo
			transform.eulerAngles = new Vector3 (transform.rotation.eulerAngles.x, -90f, transform.rotation.eulerAngles.z);
		}

		//rotacja w kierunku kursora
		float aimingAngle = Mathf.Atan2 ((mouseWorldPosition.y - AimFromPoint.transform.position.y), (mouseWorldPosition.x - AimFromPoint.transform.position.x)) * Mathf.Rad2Deg + 90;

		if (aimingAngle > 180f) {
			aimingAngle = 360f - aimingAngle;
		} else if (aimingAngle < 0f)
			aimingAngle = Mathf.Abs (aimingAngle);

		anim.SetFloat ("AimLevel", aimingAngle);
	}

	void ArmHero()
	{
		if (Input.GetMouseButton (1)) {
			if (!isArmed)
				anim.SetTrigger ("ArmHero");
			isArmed = true;
			anim.SetBool ("IsArmed", true);
		} else {
			isArmed = false;
			anim.SetBool ("IsArmed", false);
		}
	}

	void FlyingRayCheck()
	{
		RaycastHit hit;

		if (Physics.Raycast (transform.position, Vector3.down, out hit, flyingRayMaxDistance)) {
			isFlying = false;
		}
	}

	public void RestartHero(bool isThisNewGame)
	{
		if (isThisNewGame) {
			livesLeft = OverallSettings._instance.heroLivesAtStart;
		}
		UILivesNumber.text = "x" + livesLeft.ToString ();
		currentHealth = OverallSettings._instance.heroMaxHealth;
		healthSlider.maxValue = OverallSettings._instance.heroMaxHealth;
		healthSlider.value = currentHealth;
		regenerationTimer = 0f;
		isRegenerationTimerOn = false;
		isDead = false;
		anim.enabled = true;
		anim.SetBool ("IsDead", false);
		fireRateTimer = 4f;
	}

	void HealthControl()
	{
		if (isRegenerationTimerOn == true)		//jeśli timer jest włączony to dodaje czas
			regenerationTimer += Time.deltaTime;

		//jeśli jest już czas i obecne zdrowie plus dodane nie przekroczy startowego to dodaje zdrowie i zeruje timer
		if (regenerationTimer >= OverallSettings._instance.regenerationTimer && !isDead) {

			if ((currentHealth + OverallSettings._instance.additionalHealth) <= OverallSettings._instance.heroMaxHealth)
				currentHealth += OverallSettings._instance.additionalHealth;
			else
				currentHealth = OverallSettings._instance.heroMaxHealth;

			healthSlider.value = currentHealth;		//aktualizacja wypełnienia paska zdrowia
			regenerationTimer = 0f;					//zerowanie timera po dodaniu zdrowia
		}

		if (currentHealth == OverallSettings._instance.heroMaxHealth) 	//jeśli obecne zdrowie równa się startowemu to timer jest wyłączany
			isRegenerationTimerOn = false;

		if (currentHealth <= (0.5f * OverallSettings._instance.heroMaxHealth))
			DamagedRobot.SetActive (true);
		else
			DamagedRobot.SetActive (false);
	}

	public void TakeDamage(int damage)
	{
		currentHealth -= damage;
		healthSlider.value = currentHealth;
		if(currentHealth <= 0 && !isDead)
			Die ();

		isRegenerationTimerOn = true;
	}

	void Die ()
	{
		isDead = true;
		livesLeft--;
		anim.SetBool ("IsDead", true);
		StartCoroutine ("DieCoroutine");
	}

	IEnumerator DieCoroutine()
	{
		yield return new WaitForSeconds (0.4f);
		anim.enabled = false;
		yield return new WaitForSeconds (3f);
		UILivesNumber.text = "x" + livesLeft.ToString ();
		if (livesLeft <= 0)
			GameController._instance.ChangeGameState (4);
		else
			RestartHero(false);
	}

	void Shoot()
	{
		fireRateTimer += Time.deltaTime;
		
		if (isArmed && Input.GetMouseButton(0) && fireRateTimer >= 1f) {
			fireRateTimer = 0f;

			BulletStartPoint.transform.LookAt (OverallSettings._instance.BulletMiddlePoint);
			GameObject bullet = (GameObject)Instantiate (BulletPrefab, BulletStartPoint.transform.position, new Quaternion ());
			bullet.GetComponent<Projectile> ().Shoot (OverallSettings._instance.BulletMiddlePoint, OverallSettings._instance.heroBulletForce, this.gameObject);
		}
	}

	public void SetHeroPositionInEntryPortal(Vector3 entryPortalPosition)
	{
		transform.position = entryPortalPosition + new Vector3 (0f, 0.3f, 0f);
	}
}
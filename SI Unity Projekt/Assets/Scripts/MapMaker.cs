﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMaker : MonoBehaviour {

	public float zCoordOfTheMap = 0f;
	public int roomWidth = 10;
	public int roomHeight = 10;
	public GameObject horizontalBlock, upwardBlock, downwardBlock, mapParent;
	public float blocksScale = 1f;

	public float seed = 1f;
	[Range(0.5f, 20f)]
	public float noiseDivider = 10f;	//im większy, tym prostsza mapa
	[Range(0f, 5f)]
	public float noiseDistance = 0.1f;
	[Range(0f, 0.2f)]
	public float flatnessLvl = 0.1f;
	[Range(0f, 0.5f)]
	public float lowestLayerMaxHeight = 0.3f;

	public int pathwaysFindMaxAttempts = 100;
	[Range(1, 25f)]
	public int layersCount = 5;
	[Range(0, 50f)]
	public int layersMinDist;
	[Range(0, 50f)]
	public int layersBeginningsMinDist;

	public int pathwayCount = 0;
	public int pathwayMinLength = 1;
	public int pathwayMaxLength = 10;

	public GameObject PortalPrefab;
	public GameObject EntryPortal, ExitPortal;

	public GameObject SideWallPrefab;
	public Transform WallsParent;

	public Room currentMap;

	public static MapMaker _instance;

	void Start () {
		_instance = this;
		currentMap = new Room ();
	}

	public void GenerateLevelMap()
	{
		seed = Random.Range (0f, 1000f);
		currentMap.ResetTheTable();
		currentMap.DestroyTheMap ();
		currentMap.BuildTheRoom ();

		SetSideWalls ();

		StartCoroutine("SetComponentsOnScene");
	}

	IEnumerator SetComponentsOnScene()
	{
		yield return new WaitForEndOfFrame ();
		SetPortalsOnTheMap ();	//ustawienie portali
		yield return new WaitForEndOfFrame ();
		EnemiesController._instance.DestroyAllEnemies ();
		EnemiesController._instance.SetEnemiesOnTheMap (EntryPortal.transform.position, ExitPortal.transform.position);		//ustawienie wrogów
		HeroController._instance.SetHeroPositionInEntryPortal(EntryPortal.transform.position);								//ustawienie gracza
	}

	//ustawienie portali na mapie
	void SetPortalsOnTheMap()
	{
		Destroy (EntryPortal);
		Vector3 positionOfEntryPortal = FindPositionOfPortal (true);
		EntryPortal = (GameObject)Instantiate (PortalPrefab, positionOfEntryPortal, Quaternion.identity);
		EntryPortal.name = "EntryPortal";

		Destroy (ExitPortal);
		Vector3 positionOfExitPortal = FindPositionOfPortal (false);
		ExitPortal = (GameObject)Instantiate (PortalPrefab, positionOfExitPortal, Quaternion.identity);
		ExitPortal.name = "ExitPortal";
	}
		
	Vector3 FindPositionOfPortal(bool isEntryPortalNeeded)
	{
		Vector3 firstRay;

		if (isEntryPortalNeeded)
			firstRay = new Vector3 (1.5f, 2f, zCoordOfTheMap - 1f);
		else {
			float x = (roomWidth * 2) - 1f;	//środek ostatniego bloku od prawej
			float y = roomHeight + 1f;
			firstRay = new Vector3 (x, y, zCoordOfTheMap - 1f);
		}

		RaycastHit hitInfo;
		bool found = false;
		Vector3 dir;
		if (isEntryPortalNeeded)
			dir = Vector3.up;
		else
			dir = Vector3.down;

		do {
			Physics.Raycast (firstRay, dir, out hitInfo, 100f);

			if (isEntryPortalNeeded)
				firstRay.x += 1f;
			else
				firstRay.x -= 1f;

			if(hitInfo.collider.gameObject.name == "Straight(Clone)")
				found = true;
		} while (!found);

		Vector3 finalPosition = new Vector3 (hitInfo.collider.transform.position.x - 1f, hitInfo.collider.transform.position.y + 1.5f, zCoordOfTheMap - 1f);
		return finalPosition;
	}

	void SetSideWalls(){
		Vector3 leftWallPos = new Vector3 (-0.1f, 0.5f * roomHeight * blocksScale, zCoordOfTheMap - 1f);
		GameObject leftWall = (GameObject)Instantiate (SideWallPrefab, leftWallPos, Quaternion.identity, WallsParent);
		leftWall.transform.localScale = new Vector3 (0.1f, roomHeight + 2, 6f);

		Vector3 rightWallPos = new Vector3((2f * roomWidth * blocksScale) + 0.1f, 0.5f * roomHeight * blocksScale, zCoordOfTheMap - 1f);
		GameObject rightWall = (GameObject)Instantiate (SideWallPrefab, rightWallPos, new Quaternion (), WallsParent);
		rightWall.transform.localScale = new Vector3 (0.1f, roomHeight + 2, 6f);
	}
}


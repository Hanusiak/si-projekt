﻿using UnityEngine;
using System.Collections;

public class CameraMoveScript : MonoBehaviour {

	public GameObject player;
	public Vector3 offset;
	float delay = 5f;

	void FixedUpdate () {
		transform.position = Vector3.Lerp (transform.position, player.transform.position + offset, delay * Time.deltaTime);
	}


	public void SetBiggerDelay()
	{
		delay = 2f;
		StartCoroutine ("SetNormalDelay");
	}

	IEnumerator SetNormalDelay()
	{
		yield return new WaitForSeconds (5f);
		delay = 4f;
	}
}

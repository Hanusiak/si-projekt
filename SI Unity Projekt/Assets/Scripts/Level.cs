﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level {

	public int lvlNumber;
	public int lvlAdditionalHealth;
	public float lvlRegenerationTime;
	public int lvlGreenTurretNumber;
	public int lvlYellowTurretNumber;
	public int lvlRedTurretNumber;

	public Level (int number, int additionalHealth, float regenerationTime, int greenTurretNumber, int yellowTurretNumber, int redTurretNumber)
	{
		this.lvlNumber = number;
		this.lvlAdditionalHealth = additionalHealth;
		this.lvlRegenerationTime = regenerationTime;
		this.lvlGreenTurretNumber = greenTurretNumber;
		this.lvlYellowTurretNumber = yellowTurretNumber;
		this.lvlRedTurretNumber = redTurretNumber;
	}
}

﻿public class BlockType {
	
	public const byte NotSet = 0;
	public const byte Horizontal = 1;
	public const byte Upward = 2;
	public const byte Downward = 3;
	public const byte Pathway = 4;
}

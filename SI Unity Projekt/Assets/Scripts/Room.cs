﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room {

	public byte[,] tableOfValues;
	List<GameObject> blocks;

	//konstruktor - inicjacja listy gameobjectów i tabeli wartości
	public Room()
	{
		blocks = new List<GameObject> ();
		tableOfValues = new byte[MapMaker._instance.roomWidth, MapMaker._instance.roomHeight];

		ResetTheTable ();
	}

	//"Budowa pokoju" - wypełnienie tablicy i zbudowanie mapy na scenie
	public void BuildTheRoom()
	{
		GenerateTableValues ();
		GenerateRoomBasedOnTable ();
	}

	//wypełnienie tablicy odpowiednimi wartościami
	void GenerateTableValues()
	{
		CreateTheGround ();	//utworzenie podstawy mapy, najniższej warstwy
		CreateHigherLayers();	//utworzenie kolejnych warstw
	}

	//utworzenie dolnej warstwy w tablicy bajtowej
	void CreateTheGround()
	{
		List<byte> listOfBlockTypes = GenerateListOfBlockTypes (MapMaker._instance.seed);
		FillTheLayerFromTheListOfTypes (listOfBlockTypes);
	}

	//utworzenie w tablicy wartości kolejnych warstw
	void CreateHigherLayers()
	{
		List<byte> listOfBlockTypes = new List<byte>();
		List<bool> listOfPathways = new List<bool> ();
		int addedLayers = 0;
		bool reachedVerticalLimit = false;
		int currentHeight = 0;

		while (addedLayers < (MapMaker._instance.layersCount - 1) && !reachedVerticalLimit) {

			currentHeight += MapMaker._instance.layersBeginningsMinDist;
			//być może tymczasowe rozwiązanie z tym seed
			listOfBlockTypes = GenerateListOfBlockTypes (MapMaker._instance.seed + (addedLayers + 1) * 100f);
			listOfPathways = CreatePathways (MapMaker._instance.pathwayCount, MapMaker._instance.pathwayMinLength, MapMaker._instance.pathwayMaxLength);

			int result = FillTheLayerFromTheListOfTypes (listOfBlockTypes, listOfPathways, currentHeight);

			while (result != 0) {
				if (result == 2) {
					reachedVerticalLimit = true;
					break;
				}
				currentHeight++;
				result = FillTheLayerFromTheListOfTypes (listOfBlockTypes, listOfPathways, currentHeight);
			}
			addedLayers++;

		}

	}

	//generowanie szumu i ustalanie kolejnych typów klocków, które zostaną dodane do listy
	List<byte> GenerateListOfBlockTypes(float seed)
	{
		List<byte> listOfBlockTypes = new List<byte> ();

		for (int x = 0; x < tableOfValues.GetLength (0); x++) {
			float leftNoiseValue = Mathf.PerlinNoise(x/MapMaker._instance.noiseDivider + seed, 1f);
			float rightNoiseValue = Mathf.PerlinNoise((x+MapMaker._instance.noiseDistance) / MapMaker._instance.noiseDivider + seed, 1f);

			float resultOfSubtraction = rightNoiseValue - leftNoiseValue;
			if (Mathf.Abs (resultOfSubtraction) < MapMaker._instance.flatnessLvl)
				listOfBlockTypes.Add (BlockType.Horizontal);
			else if (resultOfSubtraction < 0f)
				listOfBlockTypes.Add (BlockType.Downward);
			else
				listOfBlockTypes.Add (BlockType.Upward);
		}

		return listOfBlockTypes;
	}

	//odpowiednie wypełnienie tablicy wartościami z podanej listy, zwraca 0 jeśli wszystko poszło dobrze, 1 - jeśli warstwy się nałożyły, 2 - jeśli wyszło poza zakres od góry
	int FillTheLayerFromTheListOfTypes(List<byte> listOfBlockTypes, List<bool> listOfPathways = null, int additionalHeightOfLayer = 0)
	{
		int heightOfPlatform = (int)(tableOfValues.GetLength(1) * MapMaker._instance.lowestLayerMaxHeight) + additionalHeightOfLayer;
		int tempHeight = heightOfPlatform;

		//sprawdzenie, czy wystąpiłaby kolizja warstw
		for (int x = 0; x < tableOfValues.GetLength (0); x++) {

			//jeśli w dół to obniżyć poziom
			if (listOfBlockTypes [x] == BlockType.Downward && heightOfPlatform > 0)
				heightOfPlatform--;

			//wychodzi poza zakres od góry
			if (heightOfPlatform >= tableOfValues.GetLength (1)) {
				Debug.Log ("Function FillTheLayerFromTheListOfTypes: The top of the table has been reached.");
				return 2;
			}

			for (int i = 0; i <= MapMaker._instance.layersMinDist; i++) {
				//kolizja wystąpi, jeśli element będzie miał inną wartość niż BlockType.NotSet
				if ((heightOfPlatform - i) >= 0 && tableOfValues [x, heightOfPlatform - i] != BlockType.NotSet) {
					return 1;
				}
			}
			
			//jeśli w górę to podwyższyć poziom
			if (listOfBlockTypes [x] == BlockType.Upward && heightOfPlatform < tableOfValues.GetLength(1) - 1)
				heightOfPlatform++;
		}

		//poniższa część kodu wykonuje się, gdy wiadomo, że kolizja nie zajdzie. Następują tu ewentualne modyfikacje typów platform, a także wpisanie ich "na odpowiedniej wysokości" w tablicy
		heightOfPlatform = tempHeight;

		for (int x = 0; x < tableOfValues.GetLength (0); x++) {

			//jeśli w dół to obniżyć poziom
			if (listOfBlockTypes [x] == BlockType.Downward && heightOfPlatform > 0)
				heightOfPlatform--;

			if (heightOfPlatform == 0 && listOfBlockTypes [x] == BlockType.Downward) {
				if(x > 0 && tableOfValues[x - 1, heightOfPlatform] != BlockType.NotSet)
					listOfBlockTypes [x] = BlockType.Horizontal;
			}

			if(heightOfPlatform == (tableOfValues.GetLength(1)-1) && listOfBlockTypes[x] == BlockType.Upward)
				listOfBlockTypes [x] = BlockType.Horizontal;

			// żeby nie było: /_, tylko /\
			if(x > 0 && listOfBlockTypes [x] == BlockType.Horizontal && tableOfValues[x - 1, heightOfPlatform] == BlockType.Upward)
				listOfBlockTypes [x] = BlockType.Downward;

			//porównanie odp. sobie elementów list. Jeśli wypada przerwa, to ustawienie typu BlockType.Pathway
			if (listOfPathways == null || listOfPathways [x] == true)
				tableOfValues [x, heightOfPlatform] = listOfBlockTypes [x];
			else
				tableOfValues [x, heightOfPlatform] = BlockType.Pathway;

			//jeśli w górę to podwyższyć poziom
			if (listOfBlockTypes [x] == BlockType.Upward && heightOfPlatform < tableOfValues.GetLength(1) - 1)
				heightOfPlatform++;
		}	

		return 0;
	}

	//utworzenie przerw w warstwie i zwrócenie listy wartości typu bool - element false oznacza, że powinna tam być przerwa (dalszy etap)
	List<bool> CreatePathways(int numberOfPathways, int minWidth, int maxWidth)
	{
		if (minWidth < 0)
			minWidth = 0;
		if (maxWidth > MapMaker._instance.roomWidth)
			maxWidth = MapMaker._instance.roomWidth;
		if (minWidth > maxWidth)
			Debug.LogError ("Function CreatePathways: Value of minWidth is greater than MaxWidth.");

		//utworzenie listy pomocniczej, 1 (true) - będzie element, 0 (false) - przerwa
		List<bool> testList = new List<bool>();
		for (int i = 0; i < MapMaker._instance.roomWidth; i++) {
			testList.Add (true);
		}

		int succesfullyAddedPathways = 0;
		int attemptsCount = 0;

		while (succesfullyAddedPathways < numberOfPathways && attemptsCount < MapMaker._instance.pathwaysFindMaxAttempts) {
			//losowanie szerokości
			int width = Random.Range(minWidth, maxWidth + 1);
			//losowanie punktu startowego przejścia
			int pathwayStartPoint = Random.Range (0, testList.Count - width + 1);

			//sprawdzenie, czy te miejsca są "bezpieczne"
			bool isSafe = true;
			for (int i = pathwayStartPoint; i < (pathwayStartPoint + width); i++) {
				if(testList[i] == false)	//jeśli jest przerwa w tym miejscu
					isSafe = false;
			}

			//sprawdzenie też po 2 dodatkowe z każdej strony
			//lewa strona
			if ((pathwayStartPoint - 1) >= 0 && testList [pathwayStartPoint - 1] == false)
				isSafe = false;
			if ((pathwayStartPoint - 2) >= 0 && testList [pathwayStartPoint - 2] == false)
				isSafe = false;
			//prawa strona
			if ((pathwayStartPoint + width) <= (testList.Count - 1) && testList [pathwayStartPoint + width] == false)
				isSafe = false;
			if ((pathwayStartPoint + width + 1) <= (testList.Count - 1) && testList [pathwayStartPoint + width + 1] == false)
				isSafe = false;

			if (isSafe) {	//jeśli wszystkie miejsca były "bezpieczne"
				//wstawienie przerwy do listy
				for (int i = pathwayStartPoint; i < (pathwayStartPoint + width); i++) {
					testList [i] = false;
				}
				succesfullyAddedPathways++;
			}
			attemptsCount++;
		}

		return testList;
	}

	//budowanie fizycznej mapy na podstawie tablicy wartości
	void GenerateRoomBasedOnTable()
	{
		for (int i = 0; i < tableOfValues.GetLength (0); i++) {
			for (int j = 0; j < tableOfValues.GetLength (1); j++) {
				
				if (tableOfValues [i, j] == BlockType.Horizontal || tableOfValues [i, j] == BlockType.Upward || tableOfValues [i, j] == BlockType.Downward) {
					float xPos = i * 2 * MapMaker._instance.blocksScale + 2f;
					float yPos = j * MapMaker._instance.blocksScale;

					GameObject blockToCreate = MapMaker._instance.horizontalBlock;

					switch (tableOfValues [i, j]) {
					case BlockType.Upward:
						blockToCreate = MapMaker._instance.upwardBlock;
						break;
					case BlockType.Downward:
						blockToCreate = MapMaker._instance.downwardBlock;
						break;
					}

					//obniżenie pozycji klocka, jeśli to horizontalBlock
					if (tableOfValues [i, j] == BlockType.Horizontal)
						yPos -= 1f;

					GameObject newBlock = (GameObject)MonoBehaviour.Instantiate (blockToCreate, new Vector3 (xPos, yPos, MapMaker._instance.zCoordOfTheMap), Quaternion.identity);
					newBlock.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
					newBlock.transform.parent = MapMaker._instance.mapParent.transform;
					blocks.Add (newBlock);
				}
			}
		}
	}

	//niszczenie gameobjectów i czyszczenie listy
	public void DestroyTheMap()
	{
		foreach (GameObject block in blocks)
			GameObject.Destroy (block);

		blocks = new List<GameObject> ();
	}

	//reset tabeli z wartościami
	public void ResetTheTable()
	{
		for (int i = 0; i < tableOfValues.GetLength (0); i++)
			for (int j = 0; j < tableOfValues.GetLength (1); j++)
				tableOfValues [i, j] = BlockType.NotSet;
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportalScript : MonoBehaviour {

	void OnTriggerEnter(Collider collider) {
		if(this.gameObject.name == "ExitPortal" && collider.gameObject.layer == 8)
			GameController._instance.portalAchieved = true;
	}
}
